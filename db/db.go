package db

import (
	"dns-adblock/db/models"
	"dns-adblock/libs"
	"log"
	"os"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var DB *gorm.DB

func Connect() {
	var logLevel int
	dsnSQLite := "database/database.db"
	if libs.Config.Env == "production" {
		logLevel = int(logger.Error)
	} else {
		logLevel = int(logger.Info)
	}
	var err error
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,               // Slow SQL threshold
			LogLevel:      logger.LogLevel(logLevel), // Log level
			Colorful:      false,                     // Disable color
		},
	)
	if libs.Config.Env == "production" {
		newLogger.LogMode(logger.Error)
	} else {
		newLogger.LogMode(logger.Info)
	}
	DB, err = gorm.Open(sqlite.Open(dsnSQLite), &gorm.Config{
		Logger:                                   newLogger,
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		panic(err)
	}

	// Установим пул соединений
	dbSettings, err := DB.DB()
	if err != nil {
		panic(err)
	}
	dbSettings.SetMaxIdleConns(10)
	dbSettings.SetMaxOpenConns(50)
	dbSettings.SetConnMaxLifetime(time.Minute * 10)

	// Создание БД

	DB.AutoMigrate(
		&models.Blacklist{},
		&models.Whitelist{},
	)
}
