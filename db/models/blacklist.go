package models

type Blacklist struct {
	Base
	Domain string `gorm:"column:domain;type:varchar;size:200"`
}
