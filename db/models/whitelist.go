package models

type Whitelist struct {
	Base
	Domain   string  `gorm:"column:domain;type:varchar;size:255" json:"domain"`
	Type     uint16  `gorm:"column:record_type" json:"type"`
	Priority *uint16 `gorm:"column:priority" json:"priority"`
	Weight   *uint16 `gorm:"column:weight" json:"weight"`
	Port     *uint16 `gorm:"column:port" json:"port"`
	Address  *string `gorm:"column:address;type:varchar;size:255" json:"addr"`
}
