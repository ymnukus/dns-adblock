package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type Base struct {
	ID        uuid.UUID `gorm:"type:uuid;primary_key" json:"id"`
	CreatedAt time.Time `gorm:"column:created_at" json:"createdAt"`
	UpdatedAt time.Time `gorm:"column:updated_at" json:"updatedAt"`
	DeletedAt time.Time `gorm:"column:deleted_at;index" json:"deletedAt"`
}

func (base *Base) BeforeCreate(scope *gorm.DB) (err error) {
	if base.ID == uuid.Nil {
		base.ID = uuid.NewV4()
	}
	return nil
}
