package libs

import (
	"dns-adblock/structs"
	"fmt"
	"strings"
	"time"

	"github.com/miekg/dns"
)

func PrepareMessage(req *dns.Question, res *structs.DNSRecord) (lines []dns.RR) {
	// Нашли в кэше. Сразу отдадим клиенту
	head := dns.RR_Header{
		Name:   req.Name,
		Rrtype: req.Qtype,
		Class:  req.Qclass,
	}
	now := time.Now().Local()
	switch res.Type {
	case dns.TypeA:
		for _, addr := range *res.Addr {
			if addr.Ttl.After(now) {

				line := &dns.A{
					Hdr: head,
					A:   addr.Addr,
				}
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Rrtype = dns.TypeA
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypeAAAA:
		for _, addr := range *res.Addr {
			if addr.Ttl.After(now) {
				line := &dns.AAAA{
					Hdr:  head,
					AAAA: addr.Addr,
				}
				line.Hdr.Rrtype = dns.TypeAAAA
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypeMX:
		for _, addr := range *res.Mx {
			if addr.Ttl.After(now) {
				if ([]rune(addr.Addr))[len([]rune(addr.Addr))-1] != '.' {
					addr.Addr += "."
				}
				line := &dns.MX{
					Hdr:        head,
					Mx:         addr.Addr,
					Preference: addr.Preference,
				}
				line.Hdr.Rrtype = dns.TypeMX
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypeCNAME:
		for _, addr := range *res.CName {
			if addr.Ttl.After(now) {
				if ([]rune(addr.Addr))[len([]rune(addr.Addr))-1] != '.' {
					addr.Addr += "."
				}
				line := &dns.CNAME{
					Hdr:    head,
					Target: addr.Addr,
				}
				line.Hdr.Rrtype = dns.TypeCNAME
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypeDNAME:
		for _, addr := range *res.DName {
			if addr.Ttl.After(now) {
				if ([]rune(addr.Addr))[len([]rune(addr.Addr))-1] != '.' {
					addr.Addr += "."
				}
				line := &dns.DNAME{
					Hdr:    head,
					Target: addr.Addr,
				}
				line.Hdr.Rrtype = dns.TypeDNAME
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypePTR:
		for _, addr := range *res.Ptr {
			if addr.Ttl.After(now) {
				if ([]rune(addr.Addr))[len([]rune(addr.Addr))-1] != '.' {
					addr.Addr += "."
				}
				line := &dns.PTR{
					Hdr: head,
					Ptr: addr.Addr,
				}
				line.Hdr.Rrtype = dns.TypePTR
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypeTXT:
		for _, addr := range *res.Txt {
			if addr.Ttl.After(now) {
				line := &dns.TXT{
					Hdr: head,
					Txt: strings.Split(addr.Addr, "\n"),
				}
				line.Hdr.Rrtype = dns.TypeTXT
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	case dns.TypeSRV:
		for _, addr := range *res.Srv {
			if addr.Ttl.After(now) {
				line := &dns.SRV{
					Hdr:      head,
					Target:   addr.Target,
					Priority: addr.Priority,
					Weight:   addr.Weight,
					Port:     addr.Port,
				}
				line.Hdr.Rrtype = dns.TypeSRV
				line.Hdr.Ttl = uint32(addr.Ttl.Sub(now) / time.Second)
				line.Hdr.Name = fmt.Sprintf("%s.", res.Name)
				lines = append(lines, line)
			}
		}
	}

	return
}
