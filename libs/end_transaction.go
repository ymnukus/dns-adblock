package libs

import "gorm.io/gorm"

func EndTransaction(tx *gorm.DB, err error) (errout error) {
	if err != nil {
		// Откат транзакции, так как получилось неудачно
		errout = tx.Rollback().Error
	} else {
		// Ошабок нет, значит все хорошо. Подтверждаем транзакцию
		tx.Commit()
		errout = nil
	}
	return
}
