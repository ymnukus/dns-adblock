package libs

import "github.com/miekg/dns"

func StrTypeToType(dnsTypeStr string) uint16 {

	switch dnsTypeStr {
	case "A":
		return dns.TypeA
	case "AAAA":
		return dns.TypeAAAA
	case "CNAME":
		return dns.TypeCNAME
	case "DNAME":
		return dns.TypeDNAME
	case "SRV":
		return dns.TypeSRV
	case "MX":
		return dns.TypeMX
	default:
		return dns.TypeNone
	}

}
