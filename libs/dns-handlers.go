package libs

import (
	"log"
	"sync"
	"time"

	"github.com/miekg/dns"
)

func HandlerTCP(w dns.ResponseWriter, req *dns.Msg) {
	Handler(w, req)
}

func HandlerUDP(w dns.ResponseWriter, req *dns.Msg) {
	Handler(w, req)
}

func Handler(w dns.ResponseWriter, req *dns.Msg) {
	var err error
	var lines []dns.RR
	defer w.Close()

	m := new(dns.Msg)
	m.Authoritative = true
	m.SetReply(req)

	for _, question := range req.Question {

		var ls []dns.RR
		ls, err = Question(question.Name, question.Qtype, &question)
		lines = append(lines, ls...)
		if err != nil {
			log.Println(err)
		}

		if Config.FillExtra {
			// Проверим записи типа MX и SRV и, если есть, попробуем заполнить параметр Extra
			if len(ls) > 0 {
				// var lns []dns.RR
				for _, a := range ls {
					var t interface{} = a
					switch v := (t).(type) {
					case *dns.SRV:
						var q []dns.RR
						q, err = Question(v.Target, dns.TypeA, &question)
						if err != nil {
							log.Println(err)
						}
						m.Extra = append(m.Extra, q...)
						q, err = Question(v.Target, dns.TypeAAAA, &question)
						if err != nil {
							log.Println(err)
						}
						m.Extra = append(m.Extra, q...)
					case *dns.MX:
						var q []dns.RR
						q, err = Question(v.Mx, dns.TypeA, &question)
						if err != nil {
							log.Println(err)
						}
						m.Extra = append(m.Extra, q...)

						q, err = Question(v.Mx, dns.TypeAAAA, &question)
						if err != nil {
							log.Println(err)
						}
						m.Extra = append(m.Extra, q...)
					}
				}
			}
		}
	}

	m.Answer = append(m.Answer, lines...)

	err = w.WriteMsg(m)
	if err != nil {
		log.Println(err)
	}
}

func Question(name string, qtype uint16, question *dns.Question) (lines []dns.RR, err error) {

	// Проверка в списке блокировок
	if SearchInBlackList(name, qtype) != nil {
		// Если в списке блокировки, то ничего не возвращаем, типа домена такого не существует
		return
	}

	if qtype == dns.TypeA || qtype == dns.TypeAAAA {
		if res := SearchInWhiteList(name, dns.TypeCNAME); res != nil {
			lines = append(lines, PrepareMessage(question, res)...)
			if len(*res.CName) > 0 {
				for _, cname := range *res.CName {
					var ls []dns.RR
					ls, err = Question(cname.Addr, dns.TypeA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)

					ls, err = Question(cname.Addr, dns.TypeAAAA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)
				}

			}
			if len(lines) > 0 {
				return
			}
		}
		if res := SearchInWhiteList(name, dns.TypeDNAME); res != nil {
			lines = append(lines, PrepareMessage(question, res)...)
			if len(*res.DName) > 0 {
				for _, dname := range *res.DName {
					var ls []dns.RR
					ls, err = Question(dname.Addr, dns.TypeA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)

					ls, err = Question(dname.Addr, dns.TypeAAAA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)
				}

			}
			if len(lines) > 0 {
				return
			}
		}

	}

	if res := SearchInWhiteList(name, qtype); res != nil {
		lines = append(lines, PrepareMessage(question, res)...)
		if len(lines) > 0 {
			return
		}
	}

	// Проверим есть ли в кэше данные

	if qtype == dns.TypeA || qtype == dns.TypeAAAA {
		if res := SearchInCache(name, dns.TypeCNAME); res != nil {
			lines = append(lines, PrepareMessage(question, res)...)
			if len(*res.CName) > 0 {
				for _, cname := range *res.CName {
					var ls []dns.RR
					ls, err = Question(cname.Addr, dns.TypeA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)

					ls, err = Question(cname.Addr, dns.TypeAAAA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)
				}

			}
			if len(lines) > 0 {
				return
			}
		}

	}

	if res := SearchInCache(name, qtype); res != nil {
		lines = append(lines, PrepareMessage(question, res)...)
		if len(lines) > 0 {
			return
		}
	}

	// Опросим внешний DNS для поиска записей
	msg := &dns.Msg{}
	msg.SetQuestion(name, qtype)

	c := &dns.Client{
		Net:          "udp",
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 5,
	}

	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	ch := make(chan *dns.Msg, 1)
	var wg sync.WaitGroup

	// Функция отправления запроса на внешний DNS-сервер
	requestToDNS := func(nameserver string) {
		defer wg.Done()

		m := dns.Msg{}
		m.SetQuestion(name, qtype)

		msg, _, err := c.Exchange(&m, nameserver)
		if err != nil {
			log.Printf("error: %s", err.Error())
			return
		}

		ch <- msg
	}

	for _, nameserver := range Config.Nameservers {
		wg.Add(1)

		go requestToDNS(nameserver)

		select {
		case res := <-ch:
			AddInCache(res)
		case <-ticker.C:
			continue
		}

		wg.Wait()
	}

	if qtype == dns.TypeA || qtype == dns.TypeAAAA {
		if res := SearchInCache(name, dns.TypeCNAME); res != nil {
			lines = append(lines, PrepareMessage(question, res)...)
			if len(*res.CName) > 0 {
				for _, cname := range *res.CName {
					var ls []dns.RR
					ls, err = Question(cname.Addr, dns.TypeA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)
					ls, err = Question(cname.Addr, dns.TypeAAAA, question)
					if err != nil {
						return
					}
					lines = append(lines, ls...)
				}

			}
			if len(lines) > 0 {
				return
			}
		}

	}

	if res := SearchInCache(name, qtype); res != nil {
		lines = append(lines, PrepareMessage(question, res)...)
	}

	return
}
