package libs

import (
	"time"

	"github.com/miekg/dns"
)

func UpdateTtlInWhitelist() {
	WhiteList.Mutex.Lock()
	defer WhiteList.Mutex.Unlock()

	now := time.Now().Local()
	for _, dnsRecord := range WhiteList.List {
		switch dnsRecord.Type {
		case dns.TypeA:
			if dnsRecord.Addr != nil {
				if len(*dnsRecord.Addr) > 0 {
					for i := range *dnsRecord.Addr {
						(*dnsRecord.Addr)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypeAAAA:
			if dnsRecord.Addr != nil {
				if len(*dnsRecord.Addr) > 0 {
					for i := range *dnsRecord.Addr {
						(*dnsRecord.Addr)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypeMX:
			if dnsRecord.Mx != nil {
				if len(*dnsRecord.Mx) > 0 {
					for i := range *dnsRecord.Mx {
						(*dnsRecord.Mx)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypePTR:
			if dnsRecord.Ptr != nil {
				if len(*dnsRecord.Ptr) > 0 {
					for i := range *dnsRecord.Ptr {
						(*dnsRecord.Ptr)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypeTXT:
			if dnsRecord.Txt != nil {
				if len(*dnsRecord.Txt) > 0 {
					for i := range *dnsRecord.Txt {
						(*dnsRecord.Txt)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypeCNAME:
			if dnsRecord.CName != nil {
				if len(*dnsRecord.CName) > 0 {
					for i := range *dnsRecord.CName {
						(*dnsRecord.CName)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypeDNAME:
			if dnsRecord.DName != nil {
				if len(*dnsRecord.DName) > 0 {
					for i := range *dnsRecord.DName {
						(*dnsRecord.DName)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		case dns.TypeSRV:
			if dnsRecord.Srv != nil {
				if len(*dnsRecord.Srv) > 0 {
					for i := range *dnsRecord.Srv {
						(*dnsRecord.Srv)[i].Ttl = now.Add(time.Second * time.Duration(600))
					}
				}
			}
		}
	}
}
