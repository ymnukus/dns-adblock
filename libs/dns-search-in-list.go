package libs

import (
	"dns-adblock/structs"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/miekg/dns"
)

// Найдем домен в листе блокировки
func SearchInBlackList(domain string, domainType uint16) *structs.DNSRecord {
	return SearchInList(domain, domainType, BlackList)
}

// Найдем домен в листе допущенных
func SearchInWhiteList(domain string, domainType uint16) *structs.DNSRecord {
	return SearchInList(domain, domainType, WhiteList)
}

// Найдем домен в кэше
func SearchInCache(domain string, domainType uint16) *structs.DNSRecord {
	return SearchInList(domain, domainType, CacheDomain)
}

func AddInCache(msg *dns.Msg) {
	CacheDomain.Mutex.Lock()
	defer CacheDomain.Mutex.Unlock()

	if len(msg.Answer) > 0 {

		for _, answer := range msg.Answer {
			AddInCacheLine(answer)
		}
	}

	if len(msg.Extra) > 0 {
		for _, extra := range msg.Extra {
			AddInCacheLine(extra)
		}
	}
}

func AddInCacheLine(answer dns.RR) {
	var msgi interface{} = answer
	switch v := (msgi).(type) {
	case *dns.A:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.Addr != nil {
				if len(*dnsRecord.Addr) == 0 {
					var a structs.Addr
					a.Addr = v.A
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.Addr = &[]structs.Addr{a}
				} else {
					search := false
					for i := range *dnsRecord.Addr {
						if net.IP.Equal((*dnsRecord.Addr)[i].Addr, v.A) {
							search = true
							(*dnsRecord.Addr)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.Addr
						a.Addr = v.A
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := *dnsRecord.Addr
						addrs = append(addrs, a)
						dnsRecord.Addr = &addrs
					}
				}
			} else {
				var a structs.Addr
				a.Addr = v.A
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.Addr = &[]structs.Addr{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				Addr: &[]structs.Addr{
					{
						Addr: v.A,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.AAAA:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.Addr != nil {
				if len(*dnsRecord.Addr) == 0 {
					var a structs.Addr
					a.Addr = v.AAAA
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.Addr = &[]structs.Addr{a}
				} else {
					search := false
					for i := range *dnsRecord.Addr {
						if net.IP.Equal((*dnsRecord.Addr)[i].Addr, v.AAAA) {
							search = true
							(*dnsRecord.Addr)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.Addr
						a.Addr = v.AAAA
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := *dnsRecord.Addr
						addrs = append(addrs, a)
						dnsRecord.Addr = &addrs
					}
				}
			} else {
				var a structs.Addr
				a.Addr = v.AAAA
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.Addr = &[]structs.Addr{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				Addr: &[]structs.Addr{
					{
						Addr: v.AAAA,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.MX:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.Mx != nil {
				if len(*dnsRecord.Mx) == 0 {
					var a structs.Mx
					a.Addr = v.Mx
					a.Preference = v.Preference
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.Mx = &[]structs.Mx{a}
				} else {
					search := false
					for i := range *dnsRecord.Mx {
						if (*dnsRecord.Mx)[i].Addr == v.Mx {
							search = true
							(*dnsRecord.Mx)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.Mx
						a.Addr = v.Mx
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := *dnsRecord.Mx
						addrs = append(addrs, a)
						dnsRecord.Mx = &addrs
					}
				}
			} else {
				var a structs.Mx
				a.Addr = v.Mx
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.Mx = &[]structs.Mx{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				Mx: &[]structs.Mx{
					{
						Addr:       v.Mx,
						Ttl:        time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
						Preference: v.Preference,
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.CNAME:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.CName != nil {
				if len(*dnsRecord.CName) == 0 {
					var a structs.CName
					a.Addr = v.Target
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.CName = &[]structs.CName{a}
				} else {
					search := false
					for i := range *dnsRecord.CName {
						if (*dnsRecord.CName)[i].Addr == v.Target {
							search = true
							(*dnsRecord.CName)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.CName
						a.Addr = v.Target
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := *dnsRecord.CName
						addrs = append(addrs, a)
						dnsRecord.CName = &addrs
					}
				}
			} else {
				var a structs.CName
				a.Addr = v.Target
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.CName = &[]structs.CName{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				CName: &[]structs.CName{
					{
						Addr: v.Target,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.DNAME:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.DName != nil {
				if len(*dnsRecord.DName) == 0 {
					var a structs.DName
					a.Addr = v.Target
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.DName = &[]structs.DName{a}
				} else {
					search := false
					for i := range *dnsRecord.DName {
						if (*dnsRecord.DName)[i].Addr == v.Target {
							search = true
							(*dnsRecord.DName)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.DName
						a.Addr = v.Target
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := *dnsRecord.DName
						addrs = append(addrs, a)
						dnsRecord.DName = &addrs
					}
				}
			} else {
				var a structs.DName
				a.Addr = v.Target
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.DName = &[]structs.DName{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				DName: &[]structs.DName{
					{
						Addr: v.Target,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.PTR:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.Ptr != nil {
				if len(*dnsRecord.Ptr) == 0 {
					var a structs.Ptr
					a.Addr = v.Ptr
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.Ptr = &[]structs.Ptr{a}
				} else {
					search := false
					for i := range *dnsRecord.Ptr {
						if (*dnsRecord.Ptr)[i].Addr == v.Ptr {
							search = true
							(*dnsRecord.Ptr)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.Ptr
						a.Addr = v.Ptr
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := *dnsRecord.Ptr
						addrs = append(addrs, a)
						dnsRecord.Ptr = &addrs
					}
				}
			} else {
				var a structs.Ptr
				a.Addr = v.Ptr
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.Ptr = &[]structs.Ptr{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				Ptr: &[]structs.Ptr{
					{
						Addr: v.Ptr,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.TXT:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.Txt != nil {
				if len(*dnsRecord.Txt) == 0 {
					var a structs.Txt
					a.Addr = strings.Join(v.Txt, "\n")
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.Txt = &[]structs.Txt{a}
				} else {
					search := false
					for i := range *dnsRecord.Txt {
						if (*dnsRecord.Txt)[i].Addr == strings.Join(v.Txt, "\n") {
							search = true
							(*dnsRecord.Txt)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.Txt
						a.Addr = strings.Join(v.Txt, "\n")
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := append(*dnsRecord.Txt, a)
						dnsRecord.Txt = &addrs
					}
				}
			} else {
				var a structs.Txt
				a.Addr = strings.Join(v.Txt, "\n")
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.Txt = &[]structs.Txt{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				Txt: &[]structs.Txt{
					{
						Addr: strings.Join(v.Txt, "\n"),
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	case *dns.SRV:
		domain := RegexpLastDash.ReplaceAllString(strings.Trim(v.Hdr.Name, " "), "")
		if dnsRecord := CacheDomain.List[fmt.Sprintf(`%s_%d`, domain, v.Hdr.Rrtype)]; dnsRecord != nil {
			// Добавим адрес в существующую запись
			// Найдем есть ли этот адрес уже в записи
			if dnsRecord.Srv != nil {
				if len(*dnsRecord.Srv) == 0 {
					var a structs.Srv
					a.Target = v.Target
					a.Port = v.Port
					a.Priority = v.Priority
					a.Weight = v.Weight
					a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
					dnsRecord.Srv = &[]structs.Srv{a}
				} else {
					search := false
					for i := range *dnsRecord.Srv {
						if (*dnsRecord.Srv)[i].Target == v.Target {
							search = true
							(*dnsRecord.Srv)[i].Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						}
					}
					if !search {
						var a structs.Srv
						a.Target = v.Target
						a.Port = v.Port
						a.Priority = v.Priority
						a.Weight = v.Weight
						a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
						addrs := append(*dnsRecord.Srv, a)
						dnsRecord.Srv = &addrs
					}
				}
			} else {
				var a structs.Srv
				a.Target = v.Target
				a.Port = v.Port
				a.Priority = v.Priority
				a.Weight = v.Weight
				a.Ttl = time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl))
				dnsRecord.Srv = &[]structs.Srv{a}
			}
		} else {
			dnsRecord := structs.DNSRecord{
				Name:  domain,
				Type:  v.Hdr.Rrtype,
				Class: v.Hdr.Class,
				Srv: &[]structs.Srv{
					{
						Target:   v.Target,
						Port:     v.Port,
						Priority: v.Priority,
						Weight:   v.Weight,
						Ttl:      time.Now().Local().Add(time.Second * time.Duration(v.Hdr.Ttl)),
					},
				},
			}
			CacheDomain.List[fmt.Sprintf(`%s_%d`, dnsRecord.Name, dnsRecord.Type)] = &dnsRecord
		}
	}
}

// Найдем домен в списке
func SearchInList(domain string, domainType uint16, list *structs.TypeList) *structs.DNSRecord {
	list.Mutex.RLock()
	defer list.Mutex.RUnlock()
	domain = RegexpLastDash.ReplaceAllString(strings.Trim(domain, " "), "")

	if len([]rune(domain)) == 0 {
		return nil
	}

	if val, ok := list.List[domain]; ok {
		return val
	}

	if domainType == dns.TypeA || domainType == dns.TypeAAAA {
		if val, ok := list.List[fmt.Sprintf(`%s_%d`, domain, dns.TypeCNAME)]; ok {
			return val
		}
	}
	if val, ok := list.List[fmt.Sprintf(`%s_%d`, domain, domainType)]; ok {
		return val
	}
	return nil
}
