package libs

import "regexp"

var RegexpLastComma = regexp.MustCompile(",$")
var RegexpLastDash = regexp.MustCompile(`\.$`)
var PrepareStr = regexp.MustCompile(`[\s\t]+`)

/*func InitRegexps() {

}*/
