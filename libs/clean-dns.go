package libs

import (
	"dns-adblock/structs"
	"time"

	"github.com/miekg/dns"
)

func CleanDNS() {
	CacheDomain.Mutex.Lock()
	defer CacheDomain.Mutex.Unlock()
	now := time.Now().Local()
	for key, dnsRecord := range CacheDomain.List {
		switch dnsRecord.Type {
		case dns.TypeA:
			if dnsRecord.Addr != nil {
				if len(*dnsRecord.Addr) > 0 {
					var addrs []structs.Addr
					for i := range *dnsRecord.Addr {
						if int32((*dnsRecord.Addr)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.Addr)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.Addr = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypeAAAA:
			if dnsRecord.Addr != nil {
				if len(*dnsRecord.Addr) > 0 {
					var addrs []structs.Addr
					for i := range *dnsRecord.Addr {
						if int32((*dnsRecord.Addr)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.Addr)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.Addr = &addrs
					}

				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypeMX:
			if dnsRecord.Mx != nil {
				if len(*dnsRecord.Mx) > 0 {
					var addrs []structs.Mx
					for i := range *dnsRecord.Mx {
						if int32((*dnsRecord.Mx)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.Mx)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.Mx = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypePTR:
			if dnsRecord.Mx != nil {
				if len(*dnsRecord.Ptr) > 0 {
					var addrs []structs.Ptr
					for i := range *dnsRecord.Ptr {
						if int32((*dnsRecord.Ptr)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.Ptr)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.Ptr = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypeTXT:
			if dnsRecord.Txt != nil {
				if len(*dnsRecord.Txt) > 0 {
					var addrs []structs.Txt
					for i := range *dnsRecord.Txt {
						if int32((*dnsRecord.Txt)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.Txt)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.Txt = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypeCNAME:
			if dnsRecord.CName != nil {
				if len(*dnsRecord.CName) > 0 {
					var addrs []structs.CName
					for i := range *dnsRecord.CName {
						if int32((*dnsRecord.CName)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.CName)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.CName = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypeDNAME:
			if dnsRecord.DName != nil {
				if len(*dnsRecord.DName) > 0 {
					var addrs []structs.DName
					for i := range *dnsRecord.DName {
						if int32((*dnsRecord.DName)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.DName)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.DName = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		case dns.TypeSRV:
			if dnsRecord.Srv != nil {
				if len(*dnsRecord.Srv) > 0 {
					var addrs []structs.Srv
					for i := range *dnsRecord.Srv {
						if int32((*dnsRecord.Srv)[i].Ttl.Sub(now)/time.Second) > 0 {
							addrs = append(addrs, (*dnsRecord.Srv)[i])
						}
					}
					if len(addrs) == 0 {
						delete(CacheDomain.List, key)
					} else {
						dnsRecord.Srv = &addrs
					}
				} else {
					delete(CacheDomain.List, key)
				}
			} else {
				delete(CacheDomain.List, key)
			}
		}
	}
}
