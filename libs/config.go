package libs

import (
	"dns-adblock/structs"
	"math/rand"
	"os"
	"strconv"
	"strings"
)

var Config structs.Config

func LoadConfig() {
	var exists bool
	var tmp string
	var err error

	if Config.Env, exists = os.LookupEnv("ENV"); !exists {
		Config.Env = "development"
	}

	var tmpNameservers string
	if tmpNameservers, exists = os.LookupEnv("NAMESERVERS"); !exists {
		Config.Nameservers = []string{"8.8.8.8:53"}
	} else {

		Config.Nameservers = strings.Split(RegexpLastComma.ReplaceAllString(strings.Trim(tmpNameservers, " "), ""), ",")
		if len(Config.Nameservers) == 0 {
			Config.Nameservers = []string{"8.8.8.8:53"}
		} else {
			for i := range Config.Nameservers {
				Config.Nameservers[i] = strings.Trim(Config.Nameservers[i], " ")
			}
		}
	}

	if tmp, exists = os.LookupEnv("PORT"); exists {
		Config.Port, err = strconv.Atoi(tmp)
		if err != nil {
			panic(err)
		}
	} else {
		Config.Port = 53
	}

	if tmp, exists = os.LookupEnv("WEBPORT"); exists {
		Config.WebPort, err = strconv.Atoi(tmp)
		if err != nil {
			panic(err)
		}
	} else {
		Config.Port = 3000
	}

	if tmp, exists = os.LookupEnv("FILL_EXTRA"); exists {
		if tmp == "true" || tmp == "1" {
			Config.FillExtra = true
		}
	}

	if Config.Password, exists = os.LookupEnv("PASSWORD"); !exists {
		alphabet := "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%*&"

		Config.Password = ""

		for i := 0; i < 20; i++ {
			Config.Password += string([]rune(alphabet)[rand.Intn(len(alphabet))])
		}
	}
}
