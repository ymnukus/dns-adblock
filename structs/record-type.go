package structs

import (
	"net"
	"time"
)

type DNSRecord struct {
	Name  string // Название доменного имени или его части
	Type  uint16 // Тип записи из библиотеке github.com/miekg/dns
	Class uint16 // Тип класса записи из библиотеке github.com/miekg/dns
	// Ttl   uint32 // Время жизни
	Addr  *[]Addr
	Mx    *[]Mx
	CName *[]CName
	DName *[]DName
	Ptr   *[]Ptr
	Txt   *[]Txt
	Srv   *[]Srv
}

type DNSRecordWeb struct {
	Domain string `json:"domain"`
	Type   uint16 `json:"type"`

	//Addrs *[]struct {
	Addr     *string `json:"addr"`
	Priority *uint16 `json:"priority"`
	Weight   *uint16 `json:"weight"`
	Port     *uint16 `json:"port"`
	//} `json:"addrs"`
}

type Addr struct {
	Addr net.IP    // Адреса
	Ttl  time.Time // ТТЛ
}

type Mx struct {
	Addr       string    // Адрес почтового сервера
	Preference uint16    // Предпочтение
	Ttl        time.Time // TTL
}

type CName struct {
	Addr string
	Ttl  time.Time
}

type DName struct {
	Addr string
	Ttl  time.Time
}

type Ptr struct {
	Addr string
	Ttl  time.Time
}

type Txt struct {
	Addr string
	Ttl  time.Time
}

type Srv struct {
	Priority uint16
	Weight   uint16
	Port     uint16
	Target   string
	Ttl      time.Time
}
