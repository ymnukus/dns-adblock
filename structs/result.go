package structs

// Result Результат выполнения какоу-либо операции
type Result struct {
	Result  bool        `json:"result"`
	Code    int         `json:"code"`
	Message *string     `json:"message"`
	Data    interface{} `json:"data"`
}
