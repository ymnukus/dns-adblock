package structs

type Config struct {
	Env         string
	Port        int
	Nameservers []string
	FillExtra   bool

	MigrateDB bool

	WebPort  int
	Password string
}
