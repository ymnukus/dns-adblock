package structs

import (
	"github.com/golang-jwt/jwt"
)

type JwtCustomClaims struct {
	/*ID    uuid.UUID `json:"id"`
	Login string    `json:"login"`*/
	jwt.StandardClaims
}
