package structs

import "sync"

type TypeList struct {
	Mutex sync.RWMutex
	List  map[string]*DNSRecord
}
