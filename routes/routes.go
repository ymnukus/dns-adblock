package routes

import (
	"embed"
	"encoding/json"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"dns-adblock/libs"
	"dns-adblock/routes/api"
	"dns-adblock/structs"
)

//go:embed static
var content embed.FS

func Route(e *echo.Echo) {

	e.POST("/api/login", login)

	config := middleware.JWTConfig{
		Claims:     &structs.JwtCustomClaims{},
		SigningKey: []byte("secret"),
	}

	r := e.Group("/api")
	r.Use(middleware.JWTWithConfig(config))
	api.Init(r)

	api.Init(r)

	var contentHandler = echo.WrapHandler(http.FileServer(http.FS(content)))
	var contentRewrite = middleware.Rewrite(map[string]string{"/*": "/static/$1"})
	//e.GET("/", main)
	e.GET("/*", contentHandler, contentRewrite)
	e.GET("*", contentHandler, contentRewrite)

}

// Handler
/*func main(c echo.Context) error {

	var contentRewrite = middleware.Rewrite(map[string]string{"/*": "/public/$1"})

	e.GET("/*", contentHandler, contentRewrite)

	return c.String(http.StatusOK, "Hello, World!")
}*/

func login(c echo.Context) error {
	var err error
	var authForm structs.Auth
	err = json.NewDecoder(c.Request().Body).Decode(&authForm)
	if err != nil {
		// return err
		return c.JSON(http.StatusInternalServerError, structs.Result{
			Result:  false,
			Code:    http.StatusInternalServerError,
			Message: &[]string{err.Error()}[0],
		},
		)
	}

	if authForm.Password != libs.Config.Password {
		return c.JSON(http.StatusForbidden, structs.Result{
			Result:  true,
			Code:    http.StatusForbidden,
			Message: &[]string{"Неверный пароль и/или пользователь заблокирован"}[0],
		})
	}

	// Создаем токен
	claims := &structs.JwtCustomClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, structs.Result{
			Result:  false,
			Code:    http.StatusInternalServerError,
			Message: &[]string{err.Error()}[0],
		})
	}

	// Отправляем токен
	return c.JSON(http.StatusOK, structs.Result{
		Result: true,
		Code:   http.StatusOK,
		Data:   &[]string{t}[0],
	})
}
