package blacklists

import (
	"dns-adblock/db"
	"dns-adblock/db/models"
	"dns-adblock/libs"
	"dns-adblock/structs"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

func Init(e *echo.Group) {
	e.GET("/verify", verify)
	e.GET("", list)
	e.POST("", add)
	e.DELETE("/:domain", remove)
}

func list(c echo.Context) error {
	libs.BlackList.Mutex.RLock()
	defer libs.BlackList.Mutex.RUnlock()

	len := len(libs.BlackList.List)
	res := make([]string, len)
	i := 0
	for key := range libs.BlackList.List {
		res[i] = key
		i++
	}
	return c.JSON(
		http.StatusOK,
		res,
	)
}

func add(c echo.Context) error {
	libs.BlackList.Mutex.Lock()
	defer libs.BlackList.Mutex.Unlock()

	var newDnsRecord struct {
		Domain string `json:"domain"`
	}
	err := json.NewDecoder(c.Request().Body).Decode(&newDnsRecord)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structs.Result{
			Result:  false,
			Code:    http.StatusBadRequest,
			Message: &[]string{"Error request"}[0],
		})
	}

	domain := strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(newDnsRecord.Domain, ""), ""), ""))
	if domain != "" {
		if _, ok := libs.BlackList.List[domain]; !ok {

			var err error
			tx := db.DB.Begin()

			defer func() {
				libs.EndTransaction(tx, err)
			}()

			if res := tx.Create(&models.Blacklist{
				Domain: domain,
			}); res.RowsAffected == 0 {
				err = res.Error
				return c.JSON(
					http.StatusInternalServerError,
					structs.Result{
						Code:    http.StatusInternalServerError,
						Result:  false,
						Message: &[]string{res.Error.Error()}[0],
					},
				)
			}

			libs.BlackList.List[domain] = &structs.DNSRecord{}

			return c.JSON(
				http.StatusOK,
				structs.Result{
					Code:   http.StatusOK,
					Result: true,
				})
		}
	}
	return c.JSON(
		http.StatusConflict,
		structs.Result{
			Code:   http.StatusConflict,
			Result: false,
		})
}

func remove(c echo.Context) error {
	libs.BlackList.Mutex.Lock()
	defer libs.BlackList.Mutex.Unlock()
	domain := strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("domain"), ""), ""), ""))

	if domain != "" {
		if _, ok := libs.BlackList.List[domain]; ok {
			var err error
			tx := db.DB.Begin()

			defer func() {
				libs.EndTransaction(tx, err)
			}()

			if res := tx.Delete(&models.Blacklist{}, "domain = ?", domain); res.RowsAffected == 0 {
				err = res.Error
				return c.JSON(
					http.StatusInternalServerError,
					structs.Result{
						Code:    http.StatusInternalServerError,
						Result:  false,
						Message: &[]string{res.Error.Error()}[0],
					},
				)
			}
			delete(libs.BlackList.List, domain)
			return c.JSON(
				http.StatusOK,
				structs.Result{
					Code:   http.StatusOK,
					Result: true,
				})
		}
	}
	return c.JSON(
		http.StatusNotFound,
		structs.Result{
			Code:   http.StatusNotFound,
			Result: false,
		})
}

func verify(c echo.Context) error {
	libs.BlackList.Mutex.RLock()
	defer libs.BlackList.Mutex.RUnlock()
	domain := strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.QueryParam("domain"), ""), ""), ""))
	if domain != "" {
		if _, ok := libs.BlackList.List[domain]; ok {
			return c.JSON(
				http.StatusOK,
				structs.Result{
					Code:   http.StatusOK,
					Result: true,
				})
		}
	}
	return c.JSON(
		http.StatusNotFound,
		structs.Result{
			Code:   http.StatusNotFound,
			Result: false,
		})
}
