package whitelists

import (
	"dns-adblock/db"
	"dns-adblock/db/models"
	"dns-adblock/libs"
	"dns-adblock/structs"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/miekg/dns"
)

func Init(e *echo.Group) {
	e.GET("/:type/verify/:domain", verify)
	e.GET("", list)
	e.POST("/:type", add)
	e.PUT("/:type/:domain", edit)
	e.DELETE("/:type/:domain", remove)
}

func list(c echo.Context) error {
	var err error
	tx := db.DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	var domains []models.Whitelist
	if res := tx.Find(&domains); res.RowsAffected == 0 {
		return c.JSON(
			http.StatusNotFound,
			res.Error,
		)
	}

	return c.JSON(
		http.StatusOK,
		domains,
	)
}

func add(c echo.Context) error {

	dnsTypeStr := strings.ToUpper(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("type"), ""), ""), ""))
	dnsType := libs.StrTypeToType(dnsTypeStr)
	if dnsType == 0 {
		return c.JSON(http.StatusNotFound, nil)
	}

	var newDnsRecord structs.DNSRecordWeb
	err := json.NewDecoder(c.Request().Body).Decode(&newDnsRecord)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structs.Result{
			Result:  false,
			Code:    http.StatusBadRequest,
			Message: &[]string{"Error request"}[0],
		})
	}

	newDnsRecord.Domain = strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(newDnsRecord.Domain, ""), ""), ""))

	tx := db.DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	if res := tx.First(&models.Whitelist{}, "domain = ? AND record_type = ?", newDnsRecord.Domain, dnsType); res.RowsAffected > 0 {
		return c.JSON(http.StatusConflict, nil)
	}

	whitelistRecord := models.Whitelist{
		Domain:   newDnsRecord.Domain,
		Type:     dnsType,
		Priority: newDnsRecord.Priority,
		Weight:   newDnsRecord.Weight,
		Port:     newDnsRecord.Port,
		Address:  newDnsRecord.Addr,
	}

	if res := tx.Create(&whitelistRecord); res.RowsAffected == 0 {
		c.JSON(http.StatusInternalServerError, nil)
	}

	// Подготовить запись для структуры map
	libs.WhiteList.Mutex.Lock()
	defer libs.WhiteList.Mutex.Unlock()
	dnsRecord := libs.WhiteList.List[fmt.Sprintf(`%s_%d`, newDnsRecord.Domain, dnsType)]
	if dnsRecord == nil {
		dnsRecord = &structs.DNSRecord{
			Name:  newDnsRecord.Domain,
			Type:  dnsType,
			Class: dns.ClassINET,
		}
		libs.WhiteList.List[fmt.Sprintf(`%s_%d`, newDnsRecord.Domain, dnsType)] = dnsRecord
	}
	// Проверяем адреса и добавляем, если не хватает
	switch dnsType {
	case dns.TypeA:
		a := structs.Addr{
			Addr: net.ParseIP(*newDnsRecord.Addr),
			Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
		}
		if dnsRecord.Addr == nil {
			dnsRecord.Addr = &[]structs.Addr{a}
		} else {
			addrs := *dnsRecord.Addr
			addrs = append(addrs, a)
			dnsRecord.Addr = &addrs
		}
	case dns.TypeAAAA:
		aaaa := structs.Addr{
			Addr: net.ParseIP(*newDnsRecord.Addr),
			Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
		}
		if dnsRecord.Addr == nil {
			dnsRecord.Addr = &[]structs.Addr{aaaa}
		} else {
			addrs := *dnsRecord.Addr
			addrs = append(addrs, aaaa)
			dnsRecord.Addr = &addrs
		}
	case dns.TypeMX:
		mx := structs.Mx{
			Addr:       *newDnsRecord.Addr,
			Preference: *newDnsRecord.Priority,
			Ttl:        time.Now().Local().Add(time.Second * time.Duration(600)),
		}
		if dnsRecord.Mx == nil {
			dnsRecord.Mx = &[]structs.Mx{mx}
		} else {
			addrs := *dnsRecord.Mx
			addrs = append(addrs, mx)
			dnsRecord.Mx = &addrs
		}

	case dns.TypeCNAME:
		cname := structs.CName{
			Addr: *newDnsRecord.Addr,
			Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
		}
		if dnsRecord.CName == nil {
			dnsRecord.CName = &[]structs.CName{cname}
		} else {
			addrs := *dnsRecord.CName
			addrs = append(addrs, cname)
			dnsRecord.CName = &addrs
		}
	case dns.TypeDNAME:
		dname := structs.DName{
			Addr: *newDnsRecord.Addr,
			Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
		}
		if dnsRecord.DName == nil {
			dnsRecord.DName = &[]structs.DName{dname}
		} else {
			addrs := *dnsRecord.DName
			addrs = append(addrs, dname)
			dnsRecord.DName = &addrs
		}
	case dns.TypeSRV:
		srv := structs.Srv{
			Priority: *newDnsRecord.Priority,
			Weight:   *newDnsRecord.Weight,
			Port:     *newDnsRecord.Port,
			Target:   *newDnsRecord.Addr,
			Ttl:      time.Now().Local().Add(time.Second * time.Duration(600)),
		}
		if dnsRecord.Srv == nil {
			dnsRecord.Srv = &[]structs.Srv{srv}
		} else {
			addrs := *dnsRecord.Srv
			addrs = append(addrs, srv)
			dnsRecord.Srv = &addrs
		}
	}

	//return c.JSON(http.StatusOK /*dnsRecord*/, whitelistRecord)
	return c.JSON(http.StatusOK /*dnsRecord*/, structs.Result{
		Result: true,
		Code:   http.StatusOK,
	})
}

func edit(c echo.Context) error {
	domain := strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("domain"), ""), ""), ""))
	dnsTypeStr := strings.ToUpper(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("type"), ""), ""), ""))

	if domain == "" || dnsTypeStr == "" {
		return c.JSON(http.StatusNotFound, nil)
	}

	dnsType := libs.StrTypeToType(dnsTypeStr)
	if dnsType == 0 {
		return c.JSON(http.StatusNotFound, nil)
	}

	var newDnsRecord structs.DNSRecordWeb
	err := json.NewDecoder(c.Request().Body).Decode(&newDnsRecord)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structs.Result{
			Result:  false,
			Code:    http.StatusBadRequest,
			Message: &[]string{"Error request"}[0],
		})
	}

	newDnsRecord.Domain = domain

	tx := db.DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	var webDnsRecord models.Whitelist

	if res := tx.First(&webDnsRecord, "domain = ? AND record_type = ?", newDnsRecord.Domain, dnsType); res.RowsAffected == 0 {
		return c.JSON(http.StatusNotFound, nil)
	}

	libs.WhiteList.Mutex.Lock()
	defer libs.WhiteList.Mutex.Unlock()

	dnsRecord := libs.WhiteList.List[fmt.Sprintf(`%s_%d`, newDnsRecord.Domain, dnsType)]

	if dnsRecord == nil {
		return c.JSON(http.StatusNotFound, nil)
	}

	switch dnsType {
	case dns.TypeA:
		dnsRecord.Addr = &[]structs.Addr{
			{
				Addr: net.ParseIP(*newDnsRecord.Addr),
				Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
			},
		}
		webDnsRecord.Address = newDnsRecord.Addr
	case dns.TypeAAAA:
		dnsRecord.Addr = &[]structs.Addr{
			{
				Addr: net.ParseIP(*newDnsRecord.Addr),
				Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
			},
		}
		webDnsRecord.Address = newDnsRecord.Addr
	case dns.TypeCNAME:
		dnsRecord.CName = &[]structs.CName{
			{
				Addr: *newDnsRecord.Addr,
				Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
			},
		}
		webDnsRecord.Address = newDnsRecord.Addr
	case dns.TypeDNAME:
		dnsRecord.DName = &[]structs.DName{
			{
				Addr: *newDnsRecord.Addr,
				Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
			},
		}
		webDnsRecord.Address = newDnsRecord.Addr
	case dns.TypeMX:
		dnsRecord.Mx = &[]structs.Mx{
			{
				Addr:       *newDnsRecord.Addr,
				Preference: *newDnsRecord.Priority,
				Ttl:        time.Now().Local().Add(time.Second * time.Duration(600)),
			},
		}
		webDnsRecord.Address = newDnsRecord.Addr
		webDnsRecord.Priority = newDnsRecord.Priority
	case dns.TypeSRV:
		dnsRecord.Srv = &[]structs.Srv{
			{
				Target:   *newDnsRecord.Addr,
				Priority: *newDnsRecord.Priority,
				Weight:   *newDnsRecord.Weight,
				Port:     *newDnsRecord.Port,
				Ttl:      time.Now().Local().Add(time.Second * time.Duration(600)),
			},
		}
		webDnsRecord.Address = newDnsRecord.Addr
		webDnsRecord.Priority = newDnsRecord.Priority
		webDnsRecord.Weight = newDnsRecord.Weight
		webDnsRecord.Port = newDnsRecord.Port
	default:
		return c.JSON(http.StatusBadRequest, nil)
	}

	if res := tx.Save(&webDnsRecord); res.RowsAffected == 0 {
		return c.JSON(http.StatusInternalServerError, nil)
	}

	return c.JSON(http.StatusOK, structs.Result{
		Result: true,
		Code:   http.StatusOK,
	})
}

func remove(c echo.Context) error {
	domain := strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("domain"), ""), ""), ""))
	dnsTypeStr := strings.ToUpper(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("type"), ""), ""), ""))

	if domain == "" || dnsTypeStr == "" {
		return c.JSON(http.StatusNotFound, nil)
	}

	libs.WhiteList.Mutex.Lock()
	defer libs.WhiteList.Mutex.Unlock()

	dnsType := libs.StrTypeToType(dnsTypeStr)
	if dnsType == 0 {
		return c.JSON(http.StatusNotFound, nil)
	}

	var err error
	tx := db.DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	if res := tx.Delete(&models.Whitelist{}, "domain = ? AND record_type = ?", domain, dnsType); res.RowsAffected == 0 {
		return c.JSON(http.StatusNotFound, nil)
	}

	delete(libs.WhiteList.List, fmt.Sprintf(`%s_%d`, domain, dnsType))

	return c.JSON(http.StatusOK, structs.Result{
		Result: true,
		Code:   http.StatusOK,
	})
}

func verify(c echo.Context) error {
	libs.WhiteList.Mutex.RLock()
	defer libs.WhiteList.Mutex.RUnlock()
	domain := strings.ToLower(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("domain"), ""), ""), ""))
	dnsTypeStr := strings.ToUpper(libs.PrepareStr.ReplaceAllString(libs.RegexpLastComma.ReplaceAllString(libs.PrepareStr.ReplaceAllString(c.Param("type"), ""), ""), ""))
	if domain != "" && dnsTypeStr != "" {
		dnsType := libs.StrTypeToType(dnsTypeStr)
		if dnsType == 0 {
			return c.JSON(http.StatusNotFound, nil)
		}
		if _, ok := libs.WhiteList.List[fmt.Sprintf("%s_%d", domain, dnsType)]; ok {
			return c.JSON(http.StatusOK,
				structs.Result{
					Code:   http.StatusOK,
					Result: true,
				})
		}
	}
	return c.JSON(http.StatusNotFound, nil)
}
