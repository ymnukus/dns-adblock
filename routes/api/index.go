package api

import (
	blacklists "dns-adblock/routes/api/blacklist"
	whitelists "dns-adblock/routes/api/whitelist"

	"github.com/labstack/echo/v4"
)

func Init(e *echo.Group) {
	blacklists.Init(e.Group("/blacklist"))
	whitelists.Init(e.Group("/whitelist"))
}
