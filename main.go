package main

import (
	"context"
	"dns-adblock/db"
	"dns-adblock/libs"
	"dns-adblock/middlewares"
	"dns-adblock/routes"
	"dns-adblock/structs"
	"dns-adblock/utils"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/miekg/dns"
	"github.com/robfig/cron/v3"
)

var tcpHandler *dns.ServeMux
var udpHandler *dns.ServeMux

var e *echo.Echo

func init() {

	libs.LoadConfig()

	tcpHandler = dns.NewServeMux()
	tcpHandler.HandleFunc(".", libs.HandlerTCP)

	udpHandler = dns.NewServeMux()
	udpHandler.HandleFunc(".", libs.HandlerUDP)

	libs.BlackList = &structs.TypeList{
		List:  make(map[string]*structs.DNSRecord),
		Mutex: sync.RWMutex{},
	}

	libs.WhiteList = &structs.TypeList{
		List:  make(map[string]*structs.DNSRecord),
		Mutex: sync.RWMutex{},
	}

	libs.CacheDomain = &structs.TypeList{
		List:  make(map[string]*structs.DNSRecord),
		Mutex: sync.RWMutex{},
	}

	db.Connect()

	// Подготовка списков
	utils.LoadBlacklist()
	utils.LoadWhitelist()

	e = echo.New()

	// Middleware
	middlewares.Init(e)

	// Routes
	routes.Route(e)
}

func main() {

	tcpServer4 := &dns.Server{
		Addr:         fmt.Sprintf(`:%d`, libs.Config.Port),
		Net:          "tcp4",
		Handler:      tcpHandler,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	tcpServer6 := &dns.Server{
		Addr:         fmt.Sprintf(`:%d`, libs.Config.Port),
		Net:          "tcp6",
		Handler:      tcpHandler,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	udpServer6 := &dns.Server{
		Addr:         fmt.Sprintf(`:%d`, libs.Config.Port),
		Net:          "udp6",
		Handler:      udpHandler,
		UDPSize:      65535,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}
	udpServer4 := &dns.Server{
		Addr:         fmt.Sprintf(`:%d`, libs.Config.Port),
		Net:          "udp4",
		Handler:      udpHandler,
		UDPSize:      65535,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	go func() {
		if err := tcpServer4.ListenAndServe(); err != nil {
			log.Fatal("TCP-server start failed", err.Error())
		}
	}()
	go func() {
		if err := tcpServer6.ListenAndServe(); err != nil {
			log.Fatal("TCP-server start failed", err.Error())
		}
	}()
	go func() {
		if err := udpServer6.ListenAndServe(); err != nil {
			log.Fatal("UDP-server start failed", err.Error())
		}
	}()
	go func() {
		if err := udpServer4.ListenAndServe(); err != nil {
			log.Fatal("UDP-server start failed", err.Error())
		}
	}()
	go func() {
		e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", libs.Config.WebPort)))
	}()

	// Планировщик
	jakartaTime, _ := time.LoadLocation("Europe/Moscow")
	scheduler := cron.New(cron.WithLocation(jakartaTime))
	defer scheduler.Stop()

	scheduler.AddFunc("*/10 * * * *", libs.CleanDNS)
	scheduler.AddFunc("*/10 * * * *", libs.UpdateTtlInWhitelist)

	// start scheduler
	go scheduler.Start()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)
	for {
		select {
		case <-sig:
			log.Println("Terminating...")
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			if err := e.Shutdown(ctx); err != nil {
				e.Logger.Fatal(err)
			}
			tcpServer4.Listener.Close()
			tcpServer6.Listener.Close()
			udpServer6.Listener.Close()
			udpServer4.Listener.Close()
		}
	}
}
