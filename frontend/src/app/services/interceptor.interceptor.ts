import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private messageService: MessageService,
    private router: Router
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.authService.authorized) {
      request = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${this.authService.token}`)
      });
      console.log(request);
    }
    return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
      if (error.error instanceof ErrorEvent) {
      } else {
        switch (error.status) {
          case 404:
            this.messageService.add({ severity: 'error', summary: 'Ууупс...', detail: 'Данные не найдены' });
            break;
          case 403:
            this.messageService.add({ severity: 'error', summary: 'Ууупс...', detail: 'Отказано в доступе' });
            this.router.navigate(['/login']);
            break;
          case 409:
            this.messageService.add({ severity: 'error', summary: 'Ууупс...', detail: 'Такие данные уже есть' });
            break;
          case 500:
            this.messageService.add({ severity: 'error', summary: 'Внутренняя ошибка сервера', detail: error.statusText });
            break;
          default:
            this.messageService.add({ severity: 'error', summary: 'Ну всё...', detail: `${error.statusText}: ${error.message}` });
        }
      }
      return throwError(() => error);
    }));
  }
}
