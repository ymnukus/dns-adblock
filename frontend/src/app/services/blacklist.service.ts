import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IBlacklist } from '../interfaces/blacklist';
import { IResult } from '../interfaces/result';

@Injectable({
  providedIn: 'root'
})
export class BlacklistService {

  constructor(
    private http: HttpClient
  ) { }

  fetch(): Observable<string[]> {
    return this.http.get<string[]>('/api/blacklist');
  }

  remove(domain: string): Observable<IResult> {
    return this.http.delete<IResult>(`/api/blacklist/${domain}`)
  }

  create(domain: string): Observable<IResult> {
    const dmn: IBlacklist = {
      domain: domain.trim().toLocaleLowerCase()
    };
    if (dmn.domain == '') {
      return of({
        result: false,
        code: 409
      } as IResult);
    }
    return this.http.post<IResult>(`/api/blacklist`, dmn);
  }
}
