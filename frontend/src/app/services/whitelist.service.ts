import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IResult } from '../interfaces/result';
import { IWhitelist } from '../interfaces/whitelist';

@Injectable({
  providedIn: 'root'
})
export class WhitelistService {

  constructor(
    private http: HttpClient
  ) { }

  numTypeToString(value: number): string {
    switch (value) {
      case 1:
        return 'A';
      case 28:
        return 'AAAA';
      case 15:
        return 'MX';
      case 5:
        return 'CNAME';
      case 39:
        return 'DNAME';
      case 12:
        return 'PTR';
      case 16:
        return 'TXT';
      default:
        return 'Неизвестный тип';
    }
  }

  stringTypeToNumber(value: string): number {
    switch (value.trim().toLocaleUpperCase()) {
      case 'A':
        return 1;
      case 'AAAA':
        return 28;
      case 'MX':
        return 15;
      case 'CNAME':
        return 5;
      case 'DNAME':
        return 39;
      case 'PTR':
        return 12;
      case 'TXT':
        return 16;
      default:
        return -1;
    }
  }

  fetch(): Observable<IWhitelist[]> {
    return this.http.get<IWhitelist[]>(`/api/whitelist`);
  }

  create(domain: IWhitelist): Observable<IResult> {
    console.log(domain);
    return this.http.post<IResult>(`/api/whitelist/${this.numTypeToString(domain.type).toLocaleLowerCase()}`, domain);
  }

  update(domain: IWhitelist): Observable<IResult> {
    return this.http.put<IResult>(`/api/whitelist/${this.numTypeToString(domain.type).toLocaleLowerCase()}/${domain.domain.trim().toLocaleLowerCase()}`, domain);
  }

  remove(domain: IWhitelist): Observable<IResult> {
    return this.http.delete<IResult>(`/api/whitelist/${this.numTypeToString(domain.type).toLocaleLowerCase()}/${domain.domain.trim().toLocaleLowerCase()}`);
  }
}
