import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import { IResult } from 'src/app/interfaces/result'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _token: string | null = null;

  constructor(
    private http: HttpClient
  ) { }

  get authorized(): boolean {
    return !!this._token;
  }

  get token(): string | null {
    return this._token;
  }

  authorize(password: string): Observable<IResult> {
    return this.http.post<IResult>('/api/login', { password }).pipe(tap((result: IResult) => {
      //console.log(result);
      if (result.result && result.data != null) {
        this._token = result.data as string;
      } else {
        this._token = null;
      }
      console.log(this._token);
      return of(result);
    }));
  }

}
