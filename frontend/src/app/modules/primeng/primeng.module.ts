import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastModule } from 'primeng/toast';

const modules = [
  CommonModule,
  ToastModule
];

@NgModule({
  declarations: [],
  imports: modules,
  exports: modules
})
export class PrimengModule { }
