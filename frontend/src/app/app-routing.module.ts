import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlacklistComponent } from './pages/blacklist/blacklist.component';
import { LoginComponent } from './pages/login/login.component';
import { WhitelistComponent } from './pages/whitelist/whitelist.component';
import { AuthGuard } from './services/auth-guard.guard';
import { MainTmplComponent } from './templates/main-tmpl/main-tmpl.component';

const routes: Routes = [
  {
    path: '', component: MainTmplComponent, children: [
      { path: 'login', component: LoginComponent }
    ]
  },
  {
    path: '', component: MainTmplComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard], children: [
      { path: 'blacklist', component: BlacklistComponent },
      { path: 'whitelist', component: WhitelistComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
