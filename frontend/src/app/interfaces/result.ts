export interface IResult {
    result: boolean;
    code: Number;
    message?: string;
    data?: any;
}