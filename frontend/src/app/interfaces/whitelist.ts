export interface IWhitelist {
    id?: string;
    domain: string;
    type: number;
    priority?: number;
    weight?: number;
    port?: number;
    addr: string;
}
