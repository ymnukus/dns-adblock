import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material/material.module';
import { PrimengModule } from './modules/primeng/primeng.module';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MainTmplComponent } from './templates/main-tmpl/main-tmpl.component';
import { InterceptorInterceptor } from './services/interceptor.interceptor';
import { BlacklistComponent } from './pages/blacklist/blacklist.component';
import { WhitelistComponent } from './pages/whitelist/whitelist.component';
import { EditBlacklistComponent } from './pages/blacklist/edit-blacklist/edit-blacklist.component';
import { MessageService } from 'primeng/api';
import { EditWhitelistComponent } from './pages/whitelist/edit-whitelist/edit-whitelist.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainTmplComponent,
    BlacklistComponent,
    WhitelistComponent,
    EditBlacklistComponent,
    EditWhitelistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    PrimengModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorInterceptor,
      multi: true,
    },
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
