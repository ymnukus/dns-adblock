import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MessageService } from 'primeng/api';
import { debounceTime, firstValueFrom, Subscription } from 'rxjs';
import { IResult } from 'src/app/interfaces/result';
import { IWhitelist } from 'src/app/interfaces/whitelist';
import { WhitelistService } from 'src/app/services/whitelist.service';
import { EditWhitelistComponent } from './edit-whitelist/edit-whitelist.component';

@Component({
  selector: 'app-whitelist',
  templateUrl: './whitelist.component.html',
  styleUrls: ['./whitelist.component.scss']
})
export class WhitelistComponent implements OnInit, AfterViewInit, OnDestroy {

  dataSource: MatTableDataSource<IWhitelist> = new MatTableDataSource()
  displayedColumns: string[] = [
    'domain',
    'type',
    'priority',
    'weight',
    'port',
    'address',
    'action'
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator | null = null

  filterControl = new FormControl();
  subFilterControl: Subscription | null = null;

  constructor(
    public whitelistService: WhitelistService,
    private toast: MessageService,

    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.refresh()
    this.subFilterControl = this.filterControl.valueChanges.pipe(debounceTime(500)).subscribe((value: string) => {
      this.refresh(value);
    });
  }

  ngOnDestroy(): void {
    if (this.subFilterControl != null) {
      this.subFilterControl.unsubscribe();
      this.subFilterControl = null;
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  refresh(filter?: string): void {
    firstValueFrom(this.whitelistService.fetch()).then((result: IWhitelist[]) => {
      if (filter) {
        if (filter != '') {
          const newArr: IWhitelist[] = [];
          for (const item of result) {
            if (item.domain) {
              if (item.domain.trim().toLocaleLowerCase().indexOf(filter) >= 0) {
                newArr.push(item);
              }
            }
          }
          result = newArr;
        }
      }
      this.dataSource.data = result;
    }).catch(() => {
      this.dataSource.data = [];
    });

  }

  delete(domain: IWhitelist): void {
    firstValueFrom(this.whitelistService.remove(domain)).then((result: IResult) => {
      if (result.result) {
        this.refresh();
        this.toast.add({ severity: 'success', summary: 'Успех' })
      }
    });
  }

  edit(whitelist: IWhitelist): void {
    const dialogRef = this.dialog.open(EditWhitelistComponent, {
      width: '250px',
      data: whitelist
    });

    firstValueFrom(dialogRef.afterClosed()).then(result => {
      if (result != null) {
        firstValueFrom(this.whitelistService.update(result)).then((res: IResult) => {
          if (!res.result) {
            this.toast.add({ severity: 'warn', summary: 'Что-то не так...', detail: 'Не удалось добавить домен' });
          } else {
            this.toast.add({ severity: 'success', summary: 'Успех', detail: 'Домен добавлен' });
            this.refresh();
          }
        });
      }
    });
  }

  add(): void {
    const dialogRef = this.dialog.open(EditWhitelistComponent, {
      width: '250px',
      data: null
    });

    firstValueFrom(dialogRef.afterClosed()).then(result => {
      if (result != null) {
        console.log(result);
        firstValueFrom(this.whitelistService.create(result)).then((res: IResult) => {
          if (!res.result) {
            this.toast.add({ severity: 'warn', summary: 'Что-то не так...', detail: 'Не удалось добавить домен' });
          } else {
            this.toast.add({ severity: 'success', summary: 'Успех', detail: 'Домен добавлен' });
            this.refresh();
          }
        });
      }
    });
  }

}
