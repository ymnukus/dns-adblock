import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IWhitelist } from 'src/app/interfaces/whitelist';

@Component({
  selector: 'app-edit',
  templateUrl: './edit-whitelist.component.html',
  styleUrls: ['./edit-whitelist.component.scss']
})
export class EditWhitelistComponent implements OnInit {

  form: FormGroup = new FormGroup({
    domain: new FormControl(),
    type: new FormControl(),
    priority: new FormControl(),
    weight: new FormControl(),
    port: new FormControl(),
    address: new FormControl(),
  });

  isEdit: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<EditWhitelistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IWhitelist | null,
  ) { }

  ngOnInit(): void {
    if (this.data != null) {
      this.isEdit = true;
      console.log(this.data);
      this.form.patchValue({
        domain: this.data.domain,
        address: this.data.addr,
        type: `${this.data.type}`,
        priority: `${this.data.priority}`,
        weight: `${this.data.weight}`,
        port: `${this.data.port}`
      });
      this.form.controls['domain'].disable()
    } else {
      this.isEdit = false;
      this.form.controls['domain'].enable()
    }
  }

  onCancel(): void {
    this.data = null;
    this.dialogRef.close(null);
  }

  onCreate(): void {
    let tmp: IWhitelist = {
      domain: this.form.value.domain,
      type: +this.form.value.type,
      priority: this.form.value.priority != null ? +this.form.value.priority : 0,
      weight: this.form.value.weight != null ? +this.form.value.weight : 0,
      port: this.form.value.port != null ? +this.form.value.port : 0,
      addr: this.form.value.address
    }
    this.dialogRef.close(tmp);
  }

}
