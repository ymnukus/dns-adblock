import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { firstValueFrom } from 'rxjs';
import { IResult } from 'src/app/interfaces/result';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    password: new FormControl()
  });

  error: string | undefined;

  constructor(
    private authService: AuthService,
    private toast: MessageService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  submit(): void {
    firstValueFrom(this.authService.authorize(this.form.value.password)).then((result: IResult) => {
      if (result.result) {
        this.router.navigate(['/'])
      } else {
        this.toast.add({ severity: 'error', summary: 'Отказано в доступе', detail: 'Пароль указан не верно' });
      }
    });
  }

}
