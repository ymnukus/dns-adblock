import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MessageService } from 'primeng/api';
import { debounceTime, firstValueFrom, Subscription } from 'rxjs';
import { IResult } from 'src/app/interfaces/result';
import { BlacklistService } from 'src/app/services/blacklist.service';
import { EditBlacklistComponent } from './edit-blacklist/edit-blacklist.component';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-blacklist',
  templateUrl: './blacklist.component.html',
  styleUrls: ['./blacklist.component.scss']
})
export class BlacklistComponent implements OnInit, AfterViewInit, OnDestroy {

  dataSource: MatTableDataSource<string> = new MatTableDataSource()
  displayedColumns: string[] = ['domain', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator | null = null

  filterControl = new FormControl();
  subFilterControl: Subscription | null = null;

  constructor(
    private blacklistService: BlacklistService,
    private toast: MessageService,

    public dialog: MatDialog
  ) { }

  refresh(filter?: string): void {

    firstValueFrom(this.blacklistService.fetch()).then((result: string[]) => {
      if (filter) {
        filter = filter.trim().toLocaleLowerCase();
        let newArr: string[] = [];
        if (result) {
          if (result.length > 0) {
            for (const item of result) {
              if (item.trim().toLocaleLowerCase().indexOf(filter) >= 0) {
                newArr.push(item);
              }
            }
            result = newArr;
          }
        }
      }
      this.dataSource.data = result;
    }).catch(() => {
      this.dataSource.data = [];
    });
  }

  ngOnInit(): void {
    this.refresh()
    this.subFilterControl = this.filterControl.valueChanges.pipe(debounceTime(500)).subscribe((value: string) => {
      this.refresh(value);
    });
  }

  ngOnDestroy(): void {
    if (this.subFilterControl != null) {
      this.subFilterControl.unsubscribe();
      this.subFilterControl = null;
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  add(): void {
    const dialogRef = this.dialog.open(EditBlacklistComponent, {
      width: '250px',
      data: null
    });

    firstValueFrom(dialogRef.afterClosed()).then(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result != null) {
        firstValueFrom(this.blacklistService.create(result)).then((res: IResult) => {
          if (!res.result) {
            this.toast.add({ severity: 'warn', summary: 'Что-то не так...', detail: 'Не удалось добавить домен' });
          } else {
            this.toast.add({ severity: 'success', summary: 'Успех', detail: 'Домен добавлен' });
          }
        });
      }
    });
  }

  delete(domain: string) {
    firstValueFrom(this.blacklistService.remove(domain)).then((result: IResult) => {
      if (result.result) {
        this.refresh();
        this.toast.add({ severity: 'success', summary: 'Успех' })
      }
    })
  }
}
