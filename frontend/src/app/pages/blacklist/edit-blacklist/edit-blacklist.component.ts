import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit',
  templateUrl: './edit-blacklist.component.html',
  styleUrls: ['./edit-blacklist.component.scss']
})
export class EditBlacklistComponent implements OnInit {

  form: FormGroup = new FormGroup({
    domain: new FormControl()
  });

  constructor(
    public dialogRef: MatDialogRef<EditBlacklistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string | null,
  ) { }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.data = null;
    this.dialogRef.close(null);
  }

  onCreate(): void {
    this.dialogRef.close(this.form.value.domain);
  }

}
