package utils

import (
	"bufio"
	"dns-adblock/db"
	"dns-adblock/db/models"
	"dns-adblock/libs"
	"dns-adblock/structs"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/miekg/dns"
)

// Список блокировки
func LoadBlacklist() {
	// Загружаем из файла
	if _, err := os.Stat("database/blacklist.txt"); errors.Is(err, os.ErrNotExist) {
		return
	}

	file, err := os.Open("database/blacklist.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		str := strings.Trim(scanner.Text(), " ")
		if len([]rune(str)) > 0 {
			if ([]rune(str))[0] != '#' {
				domain := strings.ToLower(strings.Trim(str, " "))
				libs.BlackList.List[domain] = &structs.DNSRecord{}
			}
		}
	}

	// Загружаем из БД
	tx := db.DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	var domains []models.Blacklist

	if res := tx.Find(&domains); res.RowsAffected > 0 {
		// Есть список в БД. Добавим в список в ОЗУ
		for _, domain := range domains {
			if _, ok := libs.BlackList.List[domain.Domain]; !ok {
				libs.BlackList.List[domain.Domain] = &structs.DNSRecord{}
			}
		}
	}

}

// Список резолвинга
func LoadWhitelist() {
	if _, err := os.Stat("database/whitelist.txt"); errors.Is(err, os.ErrNotExist) {
		return
	}

	file, err := os.Open("database/whitelist.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		strs := strings.Split(libs.PrepareStr.ReplaceAllString(strings.Trim(scanner.Text(), " "), " "), " ")
		if len(strs) >= 3 {
			strs[0] = strings.ToLower(strs[0])
			strs[1] = strings.ToUpper(strs[1])
			strs[2] = strings.ToLower(strs[2])

			if len(strs) == 4 {
				strs[3] = strings.ToLower(strs[3])
			}

			switch strs[1] {
			case "A":
				dnsRecord := structs.DNSRecord{
					Name: strs[0],
					Type: dns.TypeA,
					Addr: &[]structs.Addr{
						{
							Addr: net.ParseIP(strs[2]),
							Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
						},
					},
				}
				libs.WhiteList.List[fmt.Sprintf(`%s_%d`, strs[0], dns.TypeA)] = &dnsRecord
			case "AAAA":
				dnsRecord := structs.DNSRecord{
					Name: strs[0],
					Type: dns.TypeAAAA,
					Addr: &[]structs.Addr{
						{
							Addr: net.ParseIP(strs[2]),
							Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
						},
					},
				}
				libs.WhiteList.List[fmt.Sprintf(`%s_%d`, strs[0], dns.TypeAAAA)] = &dnsRecord
			case "CNAME":
				dnsRecord := structs.DNSRecord{
					Name: strs[0],
					Type: dns.TypeCNAME,
					CName: &[]structs.CName{
						{
							Addr: strs[2],
							Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
						},
					},
				}
				libs.WhiteList.List[fmt.Sprintf(`%s_%d`, strs[0], dns.TypeCNAME)] = &dnsRecord
			case "DNAME":
				dnsRecord := structs.DNSRecord{
					Name: strs[0],
					Type: dns.TypeDNAME,
					DName: &[]structs.DName{
						{
							Addr: strs[2],
							Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
						},
					},
				}
				libs.WhiteList.List[fmt.Sprintf(`%s_%d`, strs[0], dns.TypeCNAME)] = &dnsRecord
			case "MX":
				if len(strs) == 4 {
					preference, err := strconv.Atoi(strs[2])
					if err != nil {
						panic(err)
					}
					dnsRecord := structs.DNSRecord{
						Name: strs[0],
						Type: dns.TypeMX,
						Mx: &[]structs.Mx{
							{
								Addr:       strs[3],
								Preference: uint16(preference),
								Ttl:        time.Now().Local().Add(time.Second * time.Duration(600)),
							},
						},
					}
					libs.WhiteList.List[fmt.Sprintf(`%s_%d`, strs[0], dns.TypeMX)] = &dnsRecord
				} else {
					err := fmt.Errorf(`error type format record for MX: %s`, strings.Join(strs, " "))
					panic(err)
				}
			case "SRV":
				if len(strs) == 6 {
					priority, err := strconv.Atoi(strs[2])
					if err != nil {
						panic(err)
					}
					weight, err := strconv.Atoi(strs[3])
					if err != nil {
						panic(err)
					}
					port, err := strconv.Atoi(strs[4])
					if err != nil {
						panic(err)
					}
					dnsRecord := structs.DNSRecord{
						Name: strs[0],
						Type: dns.TypeSRV,
						Srv: &[]structs.Srv{
							{
								Target:   strs[5],
								Priority: uint16(priority),
								Weight:   uint16(weight),
								Port:     uint16(port),
								Ttl:      time.Now().Local().Add(time.Second * time.Duration(600)),
							},
						},
					}
					libs.WhiteList.List[fmt.Sprintf(`%s_%d`, strs[0], dns.TypeMX)] = &dnsRecord
				} else {
					err := fmt.Errorf(`error type format record for SRV: %s`, strings.Join(strs, " "))
					panic(err)
				}
			default:
				err := fmt.Errorf(`unknow type record: %s`, strs[1])
				panic(err)
			}
		}
	}

	// Загружаем из БД
	tx := db.DB.Begin()

	defer func() {
		libs.EndTransaction(tx, err)
	}()

	var domains []models.Whitelist

	if res := tx.Find(&domains); res.RowsAffected > 0 {
		// Есть список в БД. Добавим в список в ОЗУ
		for _, domain := range domains {
			dnsRecord := libs.WhiteList.List[fmt.Sprintf(`%s_%d`, domain.Domain, domain.Type)]
			if dnsRecord == nil {
				// Нет в списке. Добавим
				dnsRecord = &structs.DNSRecord{}
				libs.WhiteList.List[fmt.Sprintf(`%s_%d`, domain.Domain, domain.Type)] = dnsRecord
			}
			switch domain.Type {
			case dns.TypeA:
				dnsRecord.Name = domain.Domain
				dnsRecord.Type = dns.TypeA
				dnsRecord.Class = dns.ClassINET
				dnsRecord.Addr = &[]structs.Addr{
					{
						Addr: net.ParseIP(*domain.Address),
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
					},
				}
			case dns.TypeAAAA:
				dnsRecord.Name = domain.Domain
				dnsRecord.Type = dns.TypeAAAA
				dnsRecord.Class = dns.ClassINET
				dnsRecord.Addr = &[]structs.Addr{
					{
						Addr: net.ParseIP(*domain.Address),
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
					},
				}
			case dns.TypeCNAME:
				dnsRecord.Name = domain.Domain
				dnsRecord.Type = dns.TypeCNAME
				dnsRecord.Class = dns.ClassINET
				dnsRecord.CName = &[]structs.CName{
					{
						Addr: *domain.Address,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
					},
				}
			case dns.TypeDNAME:
				dnsRecord.Name = domain.Domain
				dnsRecord.Type = dns.TypeDNAME
				dnsRecord.Class = dns.ClassINET
				dnsRecord.DName = &[]structs.DName{
					{
						Addr: *domain.Address,
						Ttl:  time.Now().Local().Add(time.Second * time.Duration(600)),
					},
				}
			case dns.TypeMX:
				dnsRecord.Name = domain.Domain
				dnsRecord.Type = dns.TypeMX
				dnsRecord.Class = dns.ClassINET
				dnsRecord.Mx = &[]structs.Mx{
					{
						Addr:       *domain.Address,
						Preference: *domain.Priority,
						Ttl:        time.Now().Local().Add(time.Second * time.Duration(600)),
					},
				}
			case dns.TypeSRV:
				dnsRecord.Name = domain.Domain
				dnsRecord.Type = dns.TypeSRV
				dnsRecord.Class = dns.ClassINET
				dnsRecord.Srv = &[]structs.Srv{
					{
						Target:   *domain.Address,
						Priority: *domain.Priority,
						Weight:   *domain.Weight,
						Port:     *domain.Port,
						Ttl:      time.Now().Local().Add(time.Second * time.Duration(600)),
					},
				}
			default:
				panic(fmt.Errorf(`unknow type: %d`, domain.Type))
			}
		}
	}
}
