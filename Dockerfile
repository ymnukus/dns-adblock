FROM node:16.13.1-bullseye as builderNode
WORKDIR /app/
COPY ./frontend/package.json ./package.json
RUN npm i
COPY ./frontend ./
RUN npm run build

FROM golang:1.17.6-bullseye AS builder
WORKDIR /app/
RUN mkdir -p routes/static
COPY ./ ./
COPY --from=builderNode /app/dist /app/routes/static
RUN go get
#RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./main ./main.go
RUN CGO_ENABLED=1 GOOS=linux go build -ldflags="-extldflags=-static" -a -installsuffix cgo -o ./main ./main.go

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /
ENV TZ=Europe/Moscow
ENV ZONEINFO=/zoneinfo.zip
COPY --from=builder /app/main /
CMD ["./main"]