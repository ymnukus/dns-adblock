#!/bin/bash
mkdir -p routes/static
rm -rf routes/static/*
touch routes/static/.gitkeep
cp frontend/dist/* routes/static/